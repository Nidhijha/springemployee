package com.ust.spring.employee;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Spring1MainEmp {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("emp.xml");
		Employee emp1 = (Employee) ctx.getBean("emp");
		System.out.println(emp1);
		
		Employee emp2 = (Employee) ctx.getBean("empl");
		System.out.println(emp2);
		
		System.out.println(emp1==emp2);

	}

}
