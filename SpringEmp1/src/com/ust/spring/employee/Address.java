package com.ust.spring.employee;

public class Address {
	private String streetName;
	private String City;
	public Address() {
	}
	public Address(String streetName, String city) {
		super();
		this.streetName = streetName;
		City = city;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	@Override
	public String toString() {
		return "Address [streetName=" + streetName + ", City=" + City + "]";
	}
	
}
